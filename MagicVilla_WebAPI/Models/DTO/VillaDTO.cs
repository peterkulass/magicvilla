﻿using System.ComponentModel.DataAnnotations;

namespace MagicVilla_WebAPI.Models.DTO
{
    public class VillaDTO
    {
        public int Id { get; set; }
        [Required]
        [MaxLength(30)]
        public String Name { get; set; }
        public int Occupancy { get; set; }
        public int Sqft { get; set; }
    }
}
